/// <reference path="babylon.2.1.d.ts" />

var BjsApp = BjsApp || {};

BjsApp.init = function(){
	//get the canvas
	var canvas = document.getElementById('renderCanvas');
	
	//create a BabylonJS engine object
	var engine = new BABYLON.Engine(canvas, true);
	
	//create scene
	var scene = new BABYLON.Scene(engine);
	
	  // This creates and positions a free camera (non-mesh)
    var camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 5, -10), scene);

    // This targets the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true);

    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;
    
    
    ///////// ****** PLane for Video Texture
    //Creation of a plane
	var plane = BABYLON.Mesh.CreatePlane("plane", 10.0, scene);
    
    
     //Plane Parameters
    plane.scaling.x = 0.2;
    plane.scaling.y = 0.2;
    plane.scaling.z = 0.2;
    plane.position.y = 2;
    
    //attach material shader with grass image
	var grass = new BABYLON.StandardMaterial('grass', scene);
	grass.diffuseTexture = new BABYLON.Texture('assets/images/grass.png', scene);
	grass.diffuseTexture.uScale = 10;
	grass.diffuseTexture.vScale = 10;
	grass.specularColor = new BABYLON.Color3(0, 0, 0);
	
	plane.material = grass;
    


	
	///////// ****** Cylindar with Emmissive Glow (for buzzer)
	var cylinder = BABYLON.Mesh.CreateCylinder('cyl', 5, 1, 3, 30, scene);
    
     //cylindar Parameters
    cylinder.scaling.x = 0.5;
    cylinder.scaling.y = 0.2;
    cylinder.scaling.z = 0.5;
    cylinder.position.y = 0;
	
	var emissiveMaterial = new BABYLON.StandardMaterial('emissiveMat', scene);
	emissiveMaterial.emissiveColor = new BABYLON.Color3(0, 1, 0);
	
	cylinder.material = emissiveMaterial;
	
	
	
	//render the scene
	engine.runRenderLoop(function(){
		scene.render();
	});
	
	//listen for resize event
	window.addEventListener('resize', function(){
		engine.resize();
	});
	
};